<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    {{-- Include file Head dari folder layouts/partials --}}
    @include('layouts.partials.head')    
</head>
<body>
    <main role="main">
        {{-- Include file navbar dari folder layouts/partials --}}
        @include('layouts.partials.nav')

        {{-- Include file header dari folder layouts/partials --}}
        @include('layouts.partials.carousel')

        <div class="container marketing">   
            {{-- Include file 3 colomn under carousel --}}
            @include('layouts.partials.column')

            {{-- Start Featurettes --}}
            <hr class="featurette-divider">

            {{-- Row Featurettes --}}
            @include('layouts.partials.rowfeaturettes')

            {{-- End Featurettes --}}
            <hr class="featurette-divider">
        </div>
            {{-- Include file footer dari folder layouts/partials --}}
            @include('layouts.partials.footer')
    </main>
    {{-- Include file script footer dari folde layouts/partials --}}
    @include('layouts.partials.script')
</body>
</html>